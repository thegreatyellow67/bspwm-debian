# BSPWM Debian 12 "bookworm" (testing branch, with BSPWM tiling wm - Version 1.2 - 07/05/2023)

Debian 12 "bookworm" (testing branch) is a post-install useful scripts collection applied upon a basic Linux Debian 11 setup, which simplifies the installation process of various packages and let you install BSPWM as tiling window manager.

# How to install

Use "Download" button at top right or via git (or just download the zip and uncompress) to download this repository, then:

    $ sudo apt install git

    create a folder in your HOME one, example:

    $ mkdir $HOME/DATA

    then:

    $ cd $HOME/DATA
	$ git clone https://gitlab.com/thegreatyellow67/bspwm-debian.git

Done!

# 1) Software installation

Only for the very first time, run the script:

	$ ./bookworm-sources
	
to change repositories (bookworm branch).

Then use the main script <b>menu</b>: 

    $ ./menu

do not forget to type "./" in front of name script.

then run bspwm-post-install to set some things for BSPWM desktop:

    $ ./bspwm-post-install

# 2) NVIDIA GPUs

(See: https://www.linuxcapable.com/how-to-install-or-upgrade-nvidia-drivers-on-debian-11-bullseye/)

	$ sudo apt install linux-headers-amd64
	$ sudo apt install nvidia-detect

	$ nvidia-detect

	$ sudo apt install {package suggested by nvidia-detect} linux-image-amd64

Once logged back in, run the “nvidia-smi” command to confirm the new Nvidia Drivers have been installed.

	$ nvidia-smi

# 3) AMD GPUs

(See: https://wiki.debian.org/AtiHowTo#AMD.2FATI_Drivers_.28amdgpu.2C_radeon.2C_r128.2C_mach64.29
      https://linuxconfig.org/how-to-install-the-latest-amd-drivers-on-debian-10-buster)

	$ sudo apt purge *nvidia*
	$ sudo apt install firmware-amd-graphics libgl1-mesa-dri libglx-mesa0 mesa-vulkan-drivers xserver-xorg-video-all

### How to Install Vulkan

Vulkan support isn’t strictly necessary, but with the widening support that its receiving in the gaming world, it can’t hurt to have, and the performance improvements it promises are substantial enough to make it worth using whenever possible. Actually, Wine and Lutris are relying on Vulkan more and more to increase compatibility and performance on a wide range of games. Install Vulkan support with the following.

	$ sudo apt install mesa-vulkan-drivers libvulkan1 vulkan-tools vulkan-utils vulkan-validationlayers

### How to Install OpenCL

If you plan on using OpenCL with your AMD card too, you should include support for it. Now, this support through Mesa isn’t the same as professional grade support. If that’s what you need, consider an officially supported distribution like Ubuntu. However, if you’re just using it for basic tasks, the Mesa support will suffice.

	$ sudo apt install mesa-opencl-icd

# 4) Enable user list at lightdm login

	$ sudo nano /usr/share/lightdm/lightdm.conf.d/01_my.conf

add the following rows:

	[SeatDefaults]
	greeter-hide-users=false

# 5) Hblock

	curl -o /tmp/hblock 'https://raw.githubusercontent.com/hectorm/hblock/v3.3.1/hblock' \
	  && echo 'd93effa9a068b82f7d4d97fdcae7a320c6f7a3ae910af8234bca870f33ec55e1  /tmp/hblock' | shasum -c \
	  && sudo mv /tmp/hblock /usr/local/bin/hblock \
	  && sudo chown 0:0 /usr/local/bin/hblock \
	  && sudo chmod 755 /usr/local/bin/hblock

Install systemd service and timer units for hblock:

https://github.com/hectorm/hblock/tree/master/resources/systemd

# 6) LSD (LSDeluxe)

https://github.com/Peltoche/lsd
https://github.com/Peltoche/lsd/releases

# 7) Virtual Machine Setup for QEMU in Linux

https://www.christitus.com/vm-setup-in-linux

# 8) Papirus Icon Theme (PPA repository)

	sudo sh -c "echo 'deb http://ppa.launchpad.net/papirus/papirus/ubuntu focal main' > /etc/apt/sources.list.d/papirus-ppa.list"
	sudo apt-get install dirmngr
	sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com E58A9D36647CAE7F
	sudo apt-get update
	sudo apt-get install papirus-icon-theme

### Hardcode icon fixer

https://github.com/Foggalong/hardcode-fixer/wiki/Instructions

### Hardcode Tray

https://github.com/bilelmoussaoui/Hardcode-Tray

Running dependencies:

- python3

- python3-gi

Pick up your favorite conversion tool:

- python3-cairosvg

- librsvg

- inkscape

- imagemagick

- svgexport

If the icons looks blury, you should try installing this package libappindicator3-1.

See #567 for reference.

Building dependencies:

- ninja

- meson (>= 0.40)

	```
	sudo sh -c "echo 'deb http://download.opensuse.org/repositories/home:/SmartFinn:/hardcode-tray/Debian_$(lsb_release -rs)/ /' > /etc/apt/sources.list.d/hardcode-tray.list"
	wget -qO- https://download.opensuse.org/repositories/home:SmartFinn:hardcode-tray/Debian_$(lsb_release -rs)/Release.key | sudo apt-key add -
	sudo apt-get update
	sudo apt-get install hardcode-tray
	```

### Papirus folders
	
https://github.com/PapirusDevelopmentTeam/papirus-folders
	
	install: wget -qO- https://git.io/papirus-folders-install | sh
	uninstall: wget -qO- https://git.io/papirus-folders-install | env uninstall=true sh
	
	howto use
	papirus-folders -l --theme Papirus-Dark (Show the current color and available colors for Papirus-Dark)
	papirus-folders -C brown --theme Papirus-Dark (Change color of folders to brown for Papirus-Dark)
	papirus-folders -D --theme Papirus-Dark (Revert to default color of folders for Papirus-Dark)
	papirus-folders -Ru (Restore the last used color from a config file)
	
### Papirus LibreOffice Theme

	add: wget -qO- https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-libreoffice-theme/master/install-papirus-root.sh | sh

	remove: wget -qO- https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-libreoffice-theme/master/remove-papirus.sh | sh
	
### Papirus Filezilla Theme

	add: wget -qO- https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-filezilla-themes/master/install.sh | sh
	
	remove: wget -qO- https://raw.githubusercontent.com/PapirusDevelopmentTeam/papirus-filezilla-themes/master/install.sh | env uninstall=true sh	

# 9) Office 2013 theme for LibreOffice

https://www.deviantart.com/charliecnr/art/Office-2013-theme-for-LibreOffice-512127527

Download link: https://onedrive.live.com/?authkey=%21ACQ9%5FIxlGddfksk&cid=0716985C019A0AB8&id=716985C019A0AB8%21676&parId=root&o=OneUp

# F A Q

<b>What can you do if the script does not execute?</b>

Since I sometimes forget to make the script executable, I include here what you can do to solve that.

A script can only run when it is marked as an executable.

	ls -al 

Above code will reveal if a script has an "x". X meaning executable.
Google "chmod" and "execute" and you will find more info.

For now if this happens, you should apply this code in the terminal and add the file name.

	chmod +x typeyourfilename

Then you can execute it by typing

	./typeyourfilename
