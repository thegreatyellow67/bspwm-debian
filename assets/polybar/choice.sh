#!/bin/bash

cd $HOME/.config/polybar
killall polybar

clear
echo "###############################################"
echo "--blocks style"
echo "###############################################"
echo ""
sh -c "./launch.sh --blocks"
sleep 20
killall polybar

clear
echo "###############################################"
echo "--colorblocks style"
echo "###############################################"
echo ""
sh -c "./launch.sh --colorblocks"
sleep 20
killall polybar

clear
echo "###############################################"
echo "--cuts style"
echo "###############################################"
echo ""
sh -c "./launch.sh --cuts"
sleep 20
killall polybar

clear
echo "###############################################"
echo "--docky style"
echo "###############################################"
echo ""
sh -c "./launch.sh --docky"
sleep 20
killall polybar

clear
echo "###############################################"
echo "--forest style"
echo "###############################################"
echo ""
sh -c "./launch.sh --forest"
sleep 20
killall polybar

clear
echo "###############################################"
echo "--grayblocks style"
echo "###############################################"
echo ""
sh -c "./launch.sh --grayblocks"
sleep 20
killall polybar

clear
echo "###############################################"
echo "--hack style"
echo "###############################################"
echo ""
sh -c "./launch.sh --hack"
sleep 20
killall polybar

clear
echo "###############################################"
echo "--material style"
echo "###############################################"
echo ""
sh -c "./launch.sh --material"
sleep 20
killall polybar

clear
echo "###############################################"
echo "--panels style"
echo "###############################################"
echo ""
sh -c "./launch.sh --panels"
sleep 20
killall polybar

clear
echo "###############################################"
echo "--shades style"
echo "###############################################"
echo ""
sh -c "./launch.sh --shades"
sleep 20
killall polybar

clear
echo "###############################################"
echo "--shapes style"
echo "###############################################"
echo ""
sh -c "./launch.sh --shapes"
sleep 20
killall polybar
