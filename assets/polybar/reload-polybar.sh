#!/bin/bash

if ( ps ax | grep updates.sh ); then
  pid=$(pgrep -f updates.sh)
  pid1=`echo $pid | awk '{ print $1 }'`
  pid2=`echo $pid | awk '{ print $2 }'`

  echo "pid1: "$pid1
  echo "pid2: "$pid2
  kill $pid1
  kill $pid2
fi

if ( ps ax | grep polybar ); then
  killall -9 polybar
fi

sh $HOME/.config/polybar/forest/launch.sh &
