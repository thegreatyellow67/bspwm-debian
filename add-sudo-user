#!/bin/bash
#
# #################################################################
#
# BSPWM Debian 12 bookworm (testing branch)
# Written to be used on 64 bits computers
#
# Author            : TheGreatYellow67 (TgY67)
# GitLab Repo       : https://gitlab.com/thegreatyellow67/
#
# Script            : add-sudo-user
# Start date        : 10/03/2023
# Last modified date: 07/05/2023
#
# #################################################################

#
# Add ANSI codes
#
. ansi-codes

#
# FUNCTION: choice
#
function choice {
	clear

	if [[ ${EUID} -ne 0 ]]; then
	   printf "\n"
       printf "${LRed} ------------------------------------${Nc}\n"
	   printf "${FRed}   This script must be run as root!${Nc}\n"
       printf "${LRed} ------------------------------------${Nc}\n"
	   printf "\n"
	   exit 1
	fi

	INSTALLED=`dpkg-query -W -f='${Status}' sudo 2>/dev/null | grep -c "ok installed"`

	if [[ ${INSTALLED} -eq 0 ]]; then
	   apt install -y sudo
	fi

    # For unicode characters see:
    # https://en.wikipedia.org/wiki/Box-drawing_character
	# https://stackoverflow.com/questions/602912/how-do-you-echo-a-4-digit-unicode-character-in-bash
    sqr_left_top="\u250c"
    sqr_right_top="\u2510"
    sqr_left_bottom="\u2514"
    sqr_right_bottom="\u2518"
    div_left="\u251c"
    div_right="\u2524"
    pipe="\u2502"
    ret="\n"

    spacer=""
    row=""
    counter=1
    line="\u2500"
    spc=" "
    until [ $counter -gt 48 ]
    do
        spacer+="${spc}"
        row+="${line}"
        ((counter++))
    done

    printf "${ret}"
    printf "${LCyan} ${sqr_left_top}${row}${sqr_right_top}${ret}"
    printf "${LCyan} ${pipe}  ${Yellow}Add new user to sudo group${LCyan}                    ${pipe}${ret}"
    printf "${LCyan} ${div_left}${row}${div_right}${ret}"
    printf "${LCyan} ${pipe}${spacer}${pipe}${ret}"
    printf "${LCyan} ${pipe}  ${Yellow}Note:${LCyan}                                         ${pipe}${ret}"
    printf "${LCyan} ${pipe}  ${Yellow}if you want to remove user from sudo group${LCyan}    ${pipe}${ret}"
    printf "${LCyan} ${pipe}  ${Yellow}use, as root, the command:${LCyan}                    ${pipe}${ret}"
    printf "${LCyan} ${pipe}${spacer}${pipe}${ret}"
    printf "${LCyan} ${pipe}  ${Yellow}gpasswd --delete [user] sudo${LCyan}                  ${pipe}${ret}"
    printf "${LCyan} ${pipe}${spacer}${pipe}${ret}"
    printf "${LCyan} ${sqr_left_bottom}${row}${sqr_right_bottom}${ret}"
    printf "${Nc}"
    printf "${ret}"

	while true; do
	  read -p " Add new user to sudo group? (Y)es, (N)o : " INPUT
	  case ${INPUT} in
		[Yy]* ) add_sudo_user; break;;
		[Nn]* ) clear; break;;
		* ) printf "\n"; printf "${FRed} Select (Y)es or (N)o${Nc}\n\n";;
	  esac
	done
}

#
# FUNCTION: add_sudo_user
#
function add_sudo_user {
	GROUP="sudo"

	printf "\n"
	read -p " Username to add? " USER
	printf "\n"

	if id -nG "$USER" | grep -qw "$GROUP"; then
		printf "${LRed} User $USER is already in group $GROUP${Nc}\n\n"
	else
		/usr/sbin/usermod -a -G ${GROUP} ${USER}
		printf "${LCyan} User $USER added to group $GROUP${Nc}\n\n"
	fi

	while true; do
	  read -p " Do you want to add another user? (Y)es, (N)o : " INPUT
	  case ${INPUT} in
		[Yy]* ) choice; break;;
		[Nn]* ) clear; break;;
		* ) printf "\n"; printf "${FRed} Select (Y)es or (N)o${Nc}\n\n";;
	  esac
	done
}

choice
