#!/bin/bash
#
# #################################################################
#
# BSPWM Debian 12 bookworm (testing branch)
# Written to be used on 64 bits computers
#
# Author            : TheGreatYellow67 (TgY67)
# GitLab Repo       : https://gitlab.com/thegreatyellow67/
#
# File              : musikcube
# Start date        : 10/03/2023
# Last modified date: 07/05/2023
#
# #################################################################

#
# Functions and ANSI codes
#

if [ ! -n "${root_folder}" ]; then
	source ../functions
	source ../ansi-codes
else
	source ${root_folder}/functions
	source ${root_folder}/ansi-codes
fi

# Check if user has sudo privileges
#
check_sudo

#
# #################################################################
#
DEB_FILE="https://github.com/clangen/musikcube/releases/download/3.0.0/musikcube_linux_3.0.0_amd64.deb"
DEB_TMP_FILENAME="musikcube-amd64.deb"
APP_NAME="Musikcube"
APP_PKG="musikcube"

clear
printf "\n"
msg_info "=> Installing ${APP_NAME}..."

INSTALLED=`dpkg-query -W -f='${Status}' ${APP_PKG} 2>/dev/null | grep -c "ok installed"`

if [[ ${INSTALLED} -eq 0 ]]; then

	if [ -f /tmp/${DEB_TMP_FILENAME} ]; then
		rm /tmp/${DEB_TMP_FILENAME}
	fi

	wget ${DEB_FILE} -O /tmp/${DEB_TMP_FILENAME}

    ## The exit status of the last command run is
    ## saved automatically in the special variable $?.
    ## Therefore, testing if its value is 0, is testing
    ## whether the last command ran correctly.
    if [[ $? > 0 ]]
    then
        msg_error "=> Package ${DEB_FILE} not found, exiting!"
    else
        sudo dpkg -i /tmp/${DEB_TMP_FILENAME}
        rm /tmp/${DEB_TMP_FILENAME}

        msg_success "=> ${APP_NAME} installed!"
        printf "\n"
    fi

else

	msg_warning "=> ${APP_NAME} is already installed in your system!"
	printf "\n"

fi

printf "\n"
read -n 1 -s -r -p " Press any key to continue"
